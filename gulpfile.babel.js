/**
 * description:
 * Gulp file to support work with boilerplate
 *
 * author : WebOnWeb
 * data : 2016-06-25
 */

'use strict';

//==============================================================================
// ALL DEPENDING
//==============================================================================
const fs = require('fs');
const gulp = require('gulp');
const path = require('path');
const yargs = require('yargs');
const moment = require('moment');
const rename = require('gulp-rename');
const template = require('gulp-template');

// SET MAIN VARIABLES
//==============================================================================
let root = './app/src';

// CREATE TASKS
//==============================================================================
// TASK TO CREATE NEW COMPONENT
gulp.task('component',() => {
  let { name, destPath, type } = _prepareParams( yargs , 'Components' );
  const parent = destPath.split('\\')[3];
  //console.log(_getFolders(_resolveToPath(`Components/${parent}`)))
  return _startTask(_setPath(`Component-${type}`), name, destPath);
});

// TASK TO CREATE NEW CONTAINER
gulp.task('container',() => {
  let { name, destPath } = _prepareParams( yargs , 'Containers' );
  return _startTask(_setPath('Container'), name, destPath);
});

// TASK TO CREATE NEW FILTER
gulp.task('module',() => {
  let { name, destPath } = _prepareParams( yargs , 'Store/Modules' );
  return _startTask(_setPath('Module'), name, destPath);
});

// TASK TO CREATE NEW REDUCER
gulp.task('reducer',() => {
  let { name, destPath } = _prepareParams( yargs , 'Redux/Reducers' );
  return _startTask(_setPath('Reducer'), name, destPath);
});

// TASK TO CREATE NEW MOCKER
gulp.task('mocker',() => {
  let { name, destPath } = _prepareParams( yargs , 'Mockers' );
  return _startTask(_setPath('Mocker'), name, destPath);
});

//==============================================================================

/**
 * Function to get folders names in directory
 *
 * @method _setPath
 * @param {String} root
 * @param {String} name
 * @access private
 */
function _getFolders(dir) {
    return fs.readdirSync(dir)
      .filter(function(file) {
        return fs.statSync(path.join(dir, file)).isDirectory();
      });
}

/**
 * Function to set main path.
 *
 * @method _setPath
 * @param {String} root
 * @param {String} name
 * @access private
 */
function _setPath(dest = ''){
  return {
    js: _resolveToPath('**/*!(.spec.js).js'),
    blankTemplates: path.join( './', 'generators', `${dest}/*.**`)
  };
}

/**
 * Function to set capitalize for string.
 *
 * @method _capitalize
 * @param {String} name
 * @access private
 */
function _capitalize(name = ''){
  return name.charAt(0).toUpperCase() + name.slice(1);
}

/**
 * Function to set path for new element.
 *
 * @method _resolveToPath
 * @param {String} root
 * @param {Object} glob
 * @param {pathCreate} name
 * @access private
 */
function _resolveToPath(pathCreate = '', glob = ''){
  return path.join(root, pathCreate, glob);
}

/**
 * Function to set path for new element.
 *
 * @method _startTask
 * @param {String} paths
 * @param {String} name
 * @param {String} destPath
 * @access private
 */
function _startTask(paths, name, destPath){
  return gulp.src(paths.blankTemplates)
    .pipe(template({
      name: name,
      upCaseName: _capitalize(name),
      dateNow : moment().format('YYYY-MM-DD')
    }))
    .pipe(gulp.dest(destPath));
}

/**
 * Function to prepare params for task.
 *
 * @method _prepareParams
 * @param {Object} yargs
 * @param {String} dest
 * @access private
 */
function _prepareParams(yargs = {}, dest = ''){
  const type = yargs.argv.type || 'dumb';
  const name = yargs.argv.name;
  const parentPath = yargs.argv.parent || '';
  const destPath = path.join(_resolveToPath(dest), parentPath, _capitalize(name));
  return { name, destPath, type };
}
