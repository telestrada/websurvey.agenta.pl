/**
* description:
* File of container <%= upCaseName %> Container
*
* data : <%= dateNow %>
*/

import html from 'html-template-tag';
import Vue from 'vue';
import { mapGetters } from 'vuex';
import store from 'Store';

// COMPONENTS //////////////////////////////////////////////////////////////////
import { } from 'Components';

// CONTAINER ///////////////////////////////////////////////////////////////////
const <%= upCaseName %> = {
  components : {  },
  store,
  computed : {
    ...mapGetters([])
  },
  template : html`
    <div class="my-container">
      Add Components here
    </div>
  `
}

export default Vue
  .component('<%= upCaseName %>', <%= upCaseName %>);
