/**
* description:
* File of container <%= upCaseName %> Vuex Module
*
* data : <%= dateNow %>
*/

import Vue from 'vue';

import { API, API_URL } from 'Config';

import {

} from './mutation-types';

const INITIAL_STATE = {

}

const mutations = {

}

const actions = {

}

const getters = {

}

// VUEX MODULE /////////////////////////////////////////////////////////////////
const Module = {
  state : INITIAL_STATE,
  mutations, actions, getters,
  strict: process.env.NODE_ENV !== 'production'
}

export default Module;
