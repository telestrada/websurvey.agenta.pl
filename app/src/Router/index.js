import VueRouter from 'vue-router';
import routes from './routes';

const router = new VueRouter({
  routes
  //mode : !__DEVELOPMENT__ ? 'history' : ''
});

export default router;
