const Survey = resolve => {
  require.ensure(['Containers/SurveyWrapper'], () => {
    resolve(require('Containers/SurveyWrapper').default)
  })
}

const NotFoundPage = resolve => {
  require.ensure(['Containers/NotFound'], () => {
    resolve(require('Containers/NotFound').default)
  })
}

const routes = [
  { path : '/:dispositionUuid', name : 'Survey', component : Survey },
  { path : '*', name : 'NotFoundPage', component : NotFoundPage }
];

export default routes;