/**
 * description:
 * Main file for entire App
 * Here is all external dependencies modules and all internal modules for app
 *
 * data : 2017-02-12
 */
import 'es6-promise/auto';
import Vue from 'vue';
import VueRouter from 'vue-router';
import lang from 'element-ui/lib/locale/lang/pl';
import locale from 'element-ui/lib/locale';
import { Button, Input, InputNumber, Col, Row, Option, Select, Radio } from 'element-ui';
import { sync } from 'vuex-router-sync';

// SET LANGUAGE FOR ELEMENT-UI /////////////////////////////////////////////////
locale.use(lang);

// VUE CONFIG //////////////////////////////////////////////////////////////////
Vue.use(VueRouter);
Vue.use(Button);
Vue.use(Input);
Vue.use(InputNumber);
Vue.use(Col);
Vue.use(Row);
Vue.use(Option);
Vue.use(Select);
Vue.use(Radio);

// INTERNAL DEPENDENCIES ///////////////////////////////////////////////////////
import App from './Containers/App';
import store from './Store';
import router from './Router'

sync(store, router);

// ADD STYLE ///////////////////////////////////////////////////////////////////
require('./Styles/style.sass');

// RUN VUE /////////////////////////////////////////////////////////////////////
new Vue({
  el : '#app',
  router,
  render : h => h(App)
})
