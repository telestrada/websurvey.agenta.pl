/**
 * description:
 * File to config bootstrap webpack.
 *
 * author : WebOnWeb
 * data : 2016-06-15
 */
/* jshint esversion: 6 */
//==============================================================================

/**
 * Object bootstrapConfig.
 */

var bootstrapConfig = {
  verbose: false,
  debug: false,
  scripts: {
    transition: false,
    alert: false,
    button: false,
    carousel: false,
    collapse: false,
    //dropdown: false,
    modal: false,
    tooltip: false,
    popover: false,
    scrollspy: false,
    tab: false,
    affix: false
  },
  styles: {
    mixins: true,
    normalize: true,
    print: false,
    glyphicons: false,
    scaffolding: true,
    type: true,
    code: false,
    grid: true,
    tables: true,
    forms: false,
    buttons: false,
    'component-animations': false,
    dropdowns: false,
    'button-groups': false,
    'input-groups': false,
    navs: false,
    navbar: false,
    breadcrumbs: true,
    pagination: true,
    pager: false,
    labels: false,
    badges: false,
    jumbotron: false,
    thumbnails: false,
    alerts: false,
    'progress-bars': false,
    media: false,
    'list-group': false,
    panels: false,
    wells: false,
    'responsive-embed': false,
    close: true,
    modals: false,
    tooltip: false,
    popovers: false,
    carousel: false,
    utilities: true,
    'responsive-utilities': true
  }
};

/*
 * Export module config.
 */
module.exports = bootstrapConfig;
