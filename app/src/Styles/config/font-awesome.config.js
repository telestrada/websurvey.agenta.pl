/**
 * description:
 * File to config font-awesome-webpack.
 *
 * author : WebOnWeb
 * data : 2016-06-15
 */
/* jshint esversion: 6 */
//==============================================================================

/**
 * Object Font Awesome Config.
 */
var FontAwesomeConfig = {
  styles: {
    mixins: true,
    core: true,
    icons: true,
    larger: true,
    path: true,
    animated: true,
  }
};

/*
 * Export module config.
 */
module.exports = FontAwesomeConfig;
