/**
* description:
* File of container Survey Vuex Module
*
* data : 2017-04-26
*/

import Vue from 'vue';
import { API, API_URL } from 'Config';

import {
  SET_CONFIG_DATA, SET_WELCOME_MESSAGE, SET_CURRENT_QUESTION,
  GET_NEXT_QUESTION_ID, SET_END_MESSAGE
} from './mutation-types';

const INITIAL_STATE = {
  config : {
    logo : null,
    backgroundColor : '#EEE',
    surveyId : null,
    surveyTitle : 'Ankieta',
    createdSurveyId : null,
    sessionId : null,
    shoudlRedirectAfterFinish: false,
    redirectLink: null,
  },
  data : {
    type : null,
    questionId : null,
    message : null,
    answers : []
  }
}

const mutations = {
  [SET_CONFIG_DATA] (state, payload) {
    state.config = {
      logo : payload.logo,
      backgroundColor  : payload.backgroundColor,
      sessionId : payload.sessionId,
      surveyId : payload.surveyId,
      shoudlRedirectAfterFinish: payload.should_redirect_after_finish,
      redirectLink: payload.redirect_link,
    }
  },
  [SET_WELCOME_MESSAGE] (state, payload) {    
    state.data = {
      ...state.data,
      message : payload.message,
      questionId : payload.nextQuestionId,
      type : payload.type
    },
    state.config = {
      ...state.config,
      createdSurveyId : payload.createdSurveyId,
      surveyTitle : payload.createdSurveyTitle
    }
  },
  [SET_CURRENT_QUESTION] (state, payload) {
    state.data = {
      ...state.data,
      type : payload.type,
      message : payload.message,
      answers : payload.answers
    }
  },
  [GET_NEXT_QUESTION_ID] (state, payload) {
    state.data = {
      ...state.data,
      questionId : payload.nextQuestionId
    }
  },
  [SET_END_MESSAGE] (state, payload) {
    state.data = {
      ...state.data,
      message : payload.message,
      questionId : null,
      type : payload.type
    }
  }
}

const actions = {
  setInitialData({ dispatch, commit, state }, routeParams = {}) {        
    const { dispositionUuid } = routeParams;
    return API.get(`${API_URL}/survey/${dispositionUuid}`)    
      .then(
        response => {          
          let configData = {
            ...response.data.data
          }
          commit(SET_CONFIG_DATA, configData);
          dispatch('setWelcomeMessage');
        }  
      )
      .catch(
        err => { 
          console.log(err) 
        }
      )
  },
  setWelcomeMessage({ commit, state }) {        
    const session_id = state.config.sessionId;
    const survey_id = state.config.surveyId;
    return API.get(`${API_URL}/create/${session_id}/${survey_id}`)    
      .then(
        response => {
          const welcomeData = response.data.data || {};
          commit(SET_WELCOME_MESSAGE, welcomeData);
        }
      )
      .catch(
        err => { 
          console.log(err) 
        }
      )
  },
  setQuestion({ commit, state }) {    
    const session_id = state.config.sessionId;
    const created_survey_id = state.config.createdSurveyId;
    const question_id = state.data.questionId;    
    if(question_id) {
      return API.get(`${API_URL}/question/${session_id}/${created_survey_id}/${question_id}`)      
        .then(
          response => {
            const questionData = response.data.data;
            commit(SET_CURRENT_QUESTION, questionData);
          }
        )
        .catch(
        err => { 
          console.log(err) 
        }
      )
    } else {
      const survey_id = state.config.surveyId;
      return API.get(`${API_URL}/end/${session_id}/${survey_id}`)      
        .then(
          response => {            
            const endData = response.data.data;
            commit(SET_END_MESSAGE, endData);
          }
        )
        .catch(
        err => { 
          console.log(err) 
        }
      )
    }
  },
  sendAnswer({ dispatch, commit, state }, answerData = {}) {    
    const session_id = state.config.sessionId;
    const created_survey_id = state.config.createdSurveyId;
    const question_id = state.data.questionId;        
    const answerId = answerData.id || null;
    const answerText = answerData.message || ( answerData || null );
    
    return API.post(`${API_URL}/answer/${session_id}/${created_survey_id}/${question_id}`, { answerId, answerText })    
      .then(
        response => {                    
          const nextQuestionData = response.data.data;
          commit(GET_NEXT_QUESTION_ID, nextQuestionData);
          dispatch('setQuestion');
        }
      )
      .catch(
        err => { 
          console.log(err) 
        }
      )
  }
}

const getters = {
  config: state => state.config,
  surveyData: state => state.data
}

// VUEX MODULE /////////////////////////////////////////////////////////////////
const Module = {
  state : INITIAL_STATE,
  mutations, actions, getters,
  strict: process.env.NODE_ENV !== 'production'
}

export default Module;
