/**
* description:
* File of container App Vuex Module
*
* data : 2017-03-13
*/

import Vue from 'vue';
import { API_URL } from 'Config';

import {
  TOGGLE_MODAL, TOGGLE_LOADING
} from './mutation-types';

const INITIAL_STATE = {
  isLoading: false,
  modal : {
    isVisible : false,
    content : {
      info: null,
      error: null
    },
    header : null,
    size : 'large',
    buttonText: 'Prześlij zgłoszenie'
  }
}

const mutations = {
  [TOGGLE_LOADING] (state, payload) {
    state.isLoading = !state.isLoading;
  },
  [TOGGLE_MODAL] (state, payload) {    
    state.modal = payload.params;
  }
}

const actions = {
  toggleLoading({ commit }) {
    commit(TOGGLE_LOADING);
  },
  toggleModal({ commit, state }, newParams = {}) {
    const params = {...state.modal, ...newParams};
    commit(TOGGLE_MODAL, { params });
  }
}

const getters = {
  modal  : state => state.modal,
  isLoading: state => state.isLoading
}

// VUEX MODULE /////////////////////////////////////////////////////////////////
const Module = {
  state : INITIAL_STATE,
  mutations, actions, getters,
  strict: process.env.NODE_ENV !== 'production'
}

export default Module;
