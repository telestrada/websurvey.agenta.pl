import Vue  from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

// MODULES /////////////////////////////////////////////////////////////////////
import App    from './Modules/App';
import Survey from './Modules/Survey';

const store = new Vuex.Store({
  modules : {
    App, Survey
  }
})

export default store;
