/**
* description:
* File of container Content Component (Dumb)
*
* data : 2017-02-18
*/

import Vue from 'vue';
import './style.sass';

// PROPS ///////////////////////////////////////////////////////////////////////
const props = [];

// COMPONENT ///////////////////////////////////////////////////////////////////
const Content = {
  props,
  template : require('./template.html')
}

// EXPORT COMPONENT ////////////////////////////////////////////////////////////
export default Vue
  .component('Content', Content);
