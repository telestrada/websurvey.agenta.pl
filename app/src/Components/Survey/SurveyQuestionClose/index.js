/**
* description:
* File of container SurveyQuestionClose Component (Dumb)
*
* data : 2017-04-25
*/

import Vue from 'vue';
import { mapActions } from 'vuex';
import './style.sass';

// PROPS ///////////////////////////////////////////////////////////////////////
const props = ['surveyData'];

// DATA ////////////////////////////////////////////////////////////////////////
const data = {
  selectedAnswer : ""
}

// COMPUTED ////////////////////////////////////////////////////////////////////
const computed = {}

// METHODS /////////////////////////////////////////////////////////////////////
const methods = {
  ...mapActions(['sendAnswer'])
}

// COMPONENT ///////////////////////////////////////////////////////////////////
const SurveyQuestionCloseComponent = {
  props,
  data : () => data,
  computed,
  methods,
  template : require('./template.html')
}

// EXPORT COMPONENT ////////////////////////////////////////////////////////////
export default Vue
  .component('SurveyQuestionClose', SurveyQuestionCloseComponent);
