/**
* description:
* File of container SurveyQuestionSlider Component (Dumb)
*
* data : 2017-04-25
*/

import Vue from 'vue';
import { mapActions } from 'vuex';
import './style.sass';
import noUiSlider from 'nouislider';

// PROPS ///////////////////////////////////////////////////////////////////////
const props = ['surveyData'];

// DATA ////////////////////////////////////////////////////////////////////////
const data = {
  selectedAnswer : null,
  minValue : 0
}

const created = function () {
  this.selectedAnswer = Math.floor(this.surveyData.answers.length / 2)
}

const mounted = function () {
  let slider = document.querySelector('.survey__slider');
  noUiSlider.create(slider, {
    start: this.selectedAnswer,
    connect: [true, false],
    range: {
      'min': this.minValue,
      'max': this.maxValue
    },
    format: {
      to: function ( value ) {
        return parseInt(value);
      },
      from: function ( value ) {
        return parseInt(value);
      }
    }
  });
  slider.noUiSlider.on('slide', () => {    
    this.selectedAnswer = slider.noUiSlider.get();
  });
}

// COMPUTED ////////////////////////////////////////////////////////////////////
const computed = {
  maxValue() {
    return this.surveyData.answers.length -1
  }
}

// METHODS /////////////////////////////////////////////////////////////////////
const methods = {
  ...mapActions(['sendAnswer'])
}

// COMPONENT ///////////////////////////////////////////////////////////////////
const SurveyQuestionSliderComponent = {
  mounted,
  created,
  props,
  data : () => data,
  computed,
  methods,
  template : require('./template.html')
}

// EXPORT COMPONENT ////////////////////////////////////////////////////////////
export default Vue
  .component('SurveyQuestionSlider', SurveyQuestionSliderComponent);
