/**
* description:
* File of container SurveyQuestionCheckbox Component (Dumb)
*
* data : 2018-03-13
*/

import Vue from 'vue';
import { mapActions } from 'vuex';
import './style.sass';

// PROPS ///////////////////////////////////////////////////////////////////////
const props = ['surveyData'];

// DATA ////////////////////////////////////////////////////////////////////////
const data = {
  selectedAnswer : ""
}

// COMPUTED ////////////////////////////////////////////////////////////////////
const computed = {}

// METHODS /////////////////////////////////////////////////////////////////////
const methods = {
  ...mapActions(['sendAnswer'])
}

// COMPONENT ///////////////////////////////////////////////////////////////////
const SurveyQuestionCheckboxComponent = {
  props,
  data : () => data,
  computed,
  methods,
  template : require('./template.html')
}

// EXPORT COMPONENT ////////////////////////////////////////////////////////////
export default Vue
  .component('SurveyQuestionCheckbox', SurveyQuestionCheckboxComponent);
