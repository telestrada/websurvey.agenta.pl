/**
* description:
* File of container SurveyQuestionOpen Component (Dumb)
*
* data : 2017-04-25
*/

import Vue from 'vue';
import { mapActions } from 'vuex';
import './style.sass';

// PROPS ///////////////////////////////////////////////////////////////////////
const props = ['surveyData'];

// DATA ////////////////////////////////////////////////////////////////////////
const data = {
  enteredText : ""
}

// COMPUTED ////////////////////////////////////////////////////////////////////
const computed = {
  charactersLeft() {
    return 250 - this.enteredText.length;
  }
}

// METHODS /////////////////////////////////////////////////////////////////////
const methods = {
  ...mapActions(['sendAnswer'])
}

// COMPONENT ///////////////////////////////////////////////////////////////////
const SurveyQuestionOpenComponent = {
  props,
  data : () => data,
  computed,
  methods,
  template : require('./template.html')
}

// EXPORT COMPONENT ////////////////////////////////////////////////////////////
export default Vue
  .component('SurveyQuestionOpen', SurveyQuestionOpenComponent);
