///// AXIOS CONFIG /////////////////////////////////////////////////////////////
import axios from 'axios';
import store from './Store';

const toggleLoading = config => {
  store.dispatch('toggleLoading');
  return config;
}

const showErrorNotification = function(error) {   
  store.dispatch('toggleLoading');       
  if (error.code == 'ECONNABORTED') {
    store.dispatch('toggleModal', {
      isVisible: true, 
      header: `Ups coś poszło nie tak...`, 
      content: { info: 'Przekroczono czas oczekiwania, proszę spróbować jeszcze raz!', error: null },
    });
  } else if (error.response && error.response.data.code == 23503) {  
    store.dispatch('toggleModal', {
      isVisible: true, 
      header: `Ups coś poszło nie tak...`, 
      content: { info: 'Niestety podana ankieta nie istnieje, upewnij się czy adres jaki podałeś jest prawidłowy i spróbuj ponownie.', error: null },
    });
  } else if (error.response && error.response.data.message === 'Ankieta została już wypełniona') { 
    store.dispatch('toggleModal', {
      isVisible: true, 
      header: `${error.response.data.message}`, 
      buttonText: 'Zamknij okno',      
    });
    const configData =  error.response.data.data;
    store.commit('SET_CONFIG_DATA', configData);             
  } else {    
    store.dispatch('toggleModal', {
      isVisible: true, 
      header: `Ups coś poszło nie tak...`, 
      content: { info: 'Wykryliśmy następujący błąd:', error: `${error.response.data.message}`},
    });             
  }
  return Promise.reject(error);
}

let api = axios.create();
api.defaults.timeout = 5000;
api.interceptors.request.use(toggleLoading, showErrorNotification);
api.interceptors.response.use(toggleLoading, showErrorNotification);

///// EXPORTS CONFIG ///////////////////////////////////////////////////////////
export const API_URL = __MOCKS__ ? 'http://localhost:3333' : 'https://api.websurvey.agenta.pl';
export const API = api;

