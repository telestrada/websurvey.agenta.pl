/**
* description:
* File of container SurveyWrapper Container
*
* data : 2017-04-25
*/

import html from 'html-template-tag';
import Vue from 'vue';
import { mapGetters } from 'vuex';
import store from 'Store';

// CONTAINER ///////////////////////////////////////////////////////////////////
const SurveyWrapper = {
  store,
  computed : {
    ...mapGetters(['config', 'surveyData', 'isLoading'])
  },
  beforeRouteEnter(to, from, next) {
    store.dispatch('setInitialData', to.params)
      .then(() => next(true))
  },
  template : html`
    <div class="survey__wrapper">
      <div class="survey__background" :style="{'background-color' : config.backgroundColor}"></div>      
      <el-row>
        <el-col :xs="24">
          <SurveyLogo :config="config"/>
        </el-col>
      </el-row>
      <el-row class="survey__content-container">
        <el-col :xs="24">
          <LoadingScreen v-if="isLoading" />
          <SurveyWelcome :survey-data="surveyData" v-if="surveyData.type === 'welcome-message'"/>
          <SurveyQuestionClose :survey-data="surveyData" v-if="surveyData.type === 'question-closed'"/>
          <SurveyQuestionOpen :survey-data="surveyData" v-if="surveyData.type === 'question-open'"/>
          <SurveyQuestionSlider :survey-data="surveyData" v-if="surveyData.type === 'question-slider'"/>
          <SurveyQuestionCheckbox :survey-data="surveyData" v-if="surveyData.type === 'question-checkbox'"/>
          <SurveyEnd :survey-data="surveyData" :survey-config="config" v-if="surveyData.type === 'end-message'" />
        </el-col>
      </el-row>
      <el-row>
        <el-col :xs="24">
          <SurveyFooter />
        </el-col>
      </el-row>
    </div>
  `
}

export default Vue
  .component('SurveyWrapper', SurveyWrapper);
