/**
* description:
* File of container NotFound Container
*
* data : 2017-05-02
*/

import html from 'html-template-tag';
import Vue from 'vue';
import { mapGetters } from 'vuex';
import store from 'Store';

// CONTAINER ///////////////////////////////////////////////////////////////////
const NotFound = {
  components : {  },
  store,
  computed : {
    ...mapGetters(['config'])
  },
  template : html`
    <div class="survey__wrapper">
      <div class="survey__background" :style="{'background-color' : config.backgroundColor}"></div>
      <el-row>
        <el-col :xs="24">
          <NotFoundPage />
        </el-col>
      </el-row>
      <el-row>
        <el-col :xs="24">
          <SurveyFooter />
        </el-col>
      </el-row>
    </div>
  `
}

export default Vue
  .component('NotFound', NotFound);
