/**
* description:
* File of container App.
* This is the root container for entire APP
*
* data : 2017-02-12
*/

import Vue from 'vue';
import { mapGetters } from 'vuex';
import store from 'Store';

///// COMPONENTS ///////////////////////////////////////////////////////////////
import { Content, Modal } from 'Components';

///// CONTAINER OBJECT /////////////////////////////////////////////////////////
const App = {
  store,
  computed : {
    ...mapGetters(['modal']),
  },
  template : `
    <div class="my-container">
      <Content />
      <Modal v-if="modal.isVisible" :modal="modal" />
    </div>
  `
}

export default Vue
  .component('App', App);
