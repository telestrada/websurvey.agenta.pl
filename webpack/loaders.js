/**
 * description:
 * File of loaders to webpack.
 */

///// DEPENDENCIES /////////////////////////////////////////////////////////////
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var extractCSS = new ExtractTextPlugin('[name].css');

///// LOADERS //////////////////////////////////////////////////////////////////
var loaders = [
  {
    test: /\.js?$/,
    exclude: /(node_modules|bower_components)/,
    loader: 'babel-loader',
    query: {
      presets : ['es2015', 'stage-0', 'stage-1'],
      plugins : [["component", [
        {
          "libraryName": "element-ui",
          "styleLibraryName": "theme-default"
        }
      ]]]
    }
  }, {
    test : /\.html$/,
    loader : 'html'
  }, {
    test: /\.css$/,
    loader: 'style-loader!css-loader'
  }, {
    test: /\.sass$/,
    exclude: /(node_modules)/,
    //loader: extractCSS.extract(['style-loader','css-loader','sass-loader','sass-resources'])
    loader: 'style-loader!css-loader!sass-loader!sass-resources'
  }, {
    test: /\.(jpe?g|png|gif|svg)$/i,
    loaders: [
      'file?hash=sha512&digest=hex&name=[hash].[ext]',
      'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
    ]
  }, {
    test: /\.json$/,
    loader: 'json-loader'
  }, {
    test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
    loader: "file"
  }, {
      test: /\.(eot|svg|ttf|woff|woff2)(\?\S*)?$/,
      loader: 'file-loader'
  }, {
    test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
    loader: "url?limit=10000&mimetype=image/svg+xml"
  }
];

module.exports = { loaders : loaders, extractCSS : extractCSS };
