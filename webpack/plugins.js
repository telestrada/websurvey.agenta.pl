/**
 * description:
 * File of plugins for webpack.
*/

///// DEPENDENCIES /////////////////////////////////////////////////////////////
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin');
var extractCSS = require('./loaders').extractCSS;

///// SET ROOT PATH ////////////////////////////////////////////////////////////
var _root = __dirname + '/..';

///// COMMON PLUGINS ///////////////////////////////////////////////////////////
var plugins = [
    extractCSS,
    new webpack.optimize.CommonsChunkPlugin("vendor", "vendor.bundle.js"),
    new HtmlWebpackPlugin({
        base        : '/',
        hash        : true,
        inject      : 'body',
        template    : 'index.ejs',
        filename    : './index.html',
        //favicon     : 'src/Styles/images/favicon.ico',
        title       : 'title',
        description : 'description',
        keywords    : 'keywords'
    }),
    new webpack.DefinePlugin({
      __DEVELOPMENT__ : (process.env.NODE_ENV === 'development') ? true : false,
      __DEVTOOLS__    : process.env.DEVTOOLS || true,
      __MOCKS__       : process.env.MOCKS || false,
      __TESTS__       : process.env.TESTS || false
    }),
    new webpack.ProvidePlugin({
       $: "jquery",
       jQuery: "jquery"
    })
];

switch(process.env.NODE_ENV) {
///// PRODUCTION PLUGINS ///////////////////////////////////////////////////////
  case 'production' :
    plugins.push(
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.AggressiveMergingPlugin(),
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimize.UglifyJsPlugin({
          mangle    : false,
          sourceMap : true,
          compress  : {
            unsafe : true,
            warnings : false,
            screw_ie8 : true,
            pure_getters : true,
            unsafe_comps : true
          },
          minimize  : true,
          output : {
            comments : false
          }
        }),
        new CleanWebpackPlugin(['*'], {
          root    : _root + '/dist',
          verbose : true,
          dry     : false
        }),
        new webpack.DefinePlugin({
          "process.env"  : {
            NODE_ENV :  '"production"'
          }
        })         
      )
  break;
  default:

  break;
}

///// EXPORT PLUGINS ///////////////////////////////////////////////////////////
module.exports = plugins;
