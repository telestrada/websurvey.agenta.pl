/**
 * description:
 * File to config webpack.
 */

///// DEPENDENCIES /////////////////////////////////////////////////////////////
var debug = process.env.NODE_ENV !== "production";
var webpack = require('webpack');
var path = require("path");
var nodeModulesPath = path.join(__dirname, 'node_modules');
var plugins = require('./plugins');
var loaders = require('./loaders').loaders;
var entry = require('./entry.json');

///// SET ROOT PATH ////////////////////////////////////////////////////////////
var _root = __dirname + '/..';

// WEBPAC CONFIG ///////////////////////////////////////////////////////////////
var WebpackConfig = {
  devServer: {
    inline : true,
    port   : 8008
  },
  context : path.resolve(__dirname + "/../app"),
  quiet : true,
  // LOAD MODULES //////////////////////////////////////////////////////////////
  module : { loaders },
  // WEBPACK DEVTOOLS //////////////////////////////////////////////////////////
  devtool : debug ? "source-map" : null,
  // SET ENTRY POINT ///////////////////////////////////////////////////////////
  entry : {
     vendor : entry.vendor,
     main   : entry.main
  },
  // SASS RESOURCES ////////////////////////////////////////////////////////////
  sassResources : [
    path.resolve(_root, 'app/src/Styles/defs/_colors.sass'),
    path.resolve(_root, 'app/src/Styles/defs/_fonts.sass'),
    path.resolve(_root, 'app/src/Styles/mixins/_mixins.sass')
  ],
  // SET RESOLVE FILES AND PATH ////////////////////////////////////////////////
  resolve : {
    root : [
      path.resolve(_root, 'app/src'),
      path.resolve(_root, 'app/src/Styles')
    ],
    modulesDirectories : [
      './app/src/Styles','./app/src/Components','./app/src/Store','./app/src',
      './app/src/Containers', './node_modules','./bower_components'
    ],
    extensions : ['', '.js', '.sass'],
    alias: {
      vue: 'vue/dist/vue.js'
    }
  },
  // SET OUTPUT POINT //////////////////////////////////////////////////////////
  output : {
    path : path.resolve(__dirname + "/../dist"),
    filename : "bundle.js"
  },
  // LOAD PLUGINS //////////////////////////////////////////////////////////////
  plugins : plugins
};

// EXPORT WEBPACK //////////////////////////////////////////////////////////////
module.exports = WebpackConfig;
